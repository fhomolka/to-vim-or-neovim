# To Vim or Neovim

A small script used to test whether the user can feel whether they are running Vim or Neovim

## How to install?

1. Clone this repo
2. Edit your `.bashrc` or `.zshrc` or wherever you keep your aliases to contain
```
vim="PATH/TO/THE/SHELL/SCRIPT.sh"
```
3. Use it

## Why?

Some people wonder if there is a noticeable difference between vim and neovim, and besides ideological reasons, should one care?

## Alright, I've used this script for a while, where do I report my findings?
You can report at to-vim-or-neovim@protonmail.com

## Thank you for participating!
