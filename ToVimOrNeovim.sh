#!/bin/bash

inputArg=$1
outputArg=""
randValue=$((RANDOM%2))
case $randValue in
	0)
		outputArg=vim
		;;
	1)
		outputArg=nvim
		;;
esac

$outputArg $1
